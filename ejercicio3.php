<!DOCTYPE html>
<html>
<head>
    <title>Matriz</title>
    <style>
        table {
            border-collapse: collapse;
        }
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>
<?php
const N = 3;
function generarMatrizCuadrada($dimension) {
    $matriz = array();
    for ($i = 0; $i < $dimension; $i++) {
        $fila = array();
        for ($j = 0; $j < $dimension; $j++) {
            $fila[] = rand(0, 9);
        }
        $matriz[] = $fila;
    }
    return $matriz;
}
function imprimirMatriz($matriz) {
    echo '<table>';
    foreach ($matriz as $fila) {
        echo '<tr>';
        foreach ($fila as $valor) {
            echo '<td>' . $valor . '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
}
function calcularSumaDiagonalPrincipal($matriz) {
    $suma = 0;
    for ($i = 0; $i < count($matriz); $i++) {
        $suma += $matriz[$i][$i];
    }
    return $suma;
}
do {
    $matriz = generarMatrizCuadrada(N);
    imprimirMatriz($matriz);
    $sumaDiagonalPrincipal = calcularSumaDiagonalPrincipal($matriz);

    echo "SUMA DE LA DIAGONAL PRINCIPAL: " . $sumaDiagonalPrincipal . "<br><br>";

    if ($sumaDiagonalPrincipal >= 10 && $sumaDiagonalPrincipal <= 15) {
        echo "- - - - - - - - - - - - - - - - - - - - - <br>";
        echo "SCRIPT FINALIZADO! SUMA ENTRE 10 Y 15!!!";
        break;
    }
} while (true);
?>
</body>
</html>
